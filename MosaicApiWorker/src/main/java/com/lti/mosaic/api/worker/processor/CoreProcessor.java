package com.lti.mosaic.api.worker.processor;

import java.io.IOException;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import in.lnt.utility.constants.LoggerConstants;
import in.lti.mosaic.api.base.bso.StatusBSO;
import in.lti.mosaic.api.base.constants.Constants;
import in.lti.mosaic.api.base.serializer.ObjectSerializationHandler;
import in.lti.mosaic.api.base.services.EventAPIService;

/**
 * 
 * @author rushi
 *
 */
public class CoreProcessor {
	
  private static final Logger logger = LoggerFactory.getLogger(CoreProcessor.class);

  /**
   * This method will parse the message and start to process the request based on the type and will
   * update the CapacityManager.currentProcesses once the processing is done
   * 
   * @param message
 * @throws IOException 
   */
  @SuppressWarnings("unchecked")
  public static void process(String message) {

    logger.debug(LoggerConstants.LOG_MAXIQAPI, ">> process");

    Map<String, String> parsedMessage =
        (Map<String, String>) ObjectSerializationHandler.toObject(message, Map.class);

    String validationType = parsedMessage.get(Constants.Request.VALIDATIONTYPE);

    logger.debug("Validation Type is {}", validationType);

    StatusBSO.insertStatusForRequestId(parsedMessage.get(Constants.Request.REQUESTID),
        parsedMessage.get(Constants.Request.VALIDATIONTYPE),
        Constants.RequestStatus.PICKEDUPFORPROCESSING,null,
        parsedMessage.get(Constants.Request.ENVIRONMENTNAME),
        parsedMessage.get(Constants.Request.DOCUMENTID), null);
    
    // Adding event for request processing has been started
    String requestId = parsedMessage.get(Constants.Request.REQUESTID);
    try {
    	EventAPIService.addEvent(requestId, Constants.RequestStatus.PICKEDUPFORPROCESSING);	
	} catch (Exception e) {
		logger.error("Error while adding event {}", e.getMessage());
	}
    

    if (StringUtils.equalsIgnoreCase(validationType, Constants.ValidationTypes.L1)) {

      ValidatorStarter.start(new L1L2Validator(parsedMessage));

    } else if (StringUtils.equalsIgnoreCase(validationType, Constants.ValidationTypes.L3A)) {

      ValidatorStarter.start(new L3AValidator(parsedMessage));

    } else if (StringUtils.equalsIgnoreCase(validationType, Constants.ValidationTypes.L3B)) {

      ValidatorStarter.start(new L3BValidator(parsedMessage));

    } else {
      
      logger.error(LoggerConstants.LOG_MAXIQAPI, "Invalid Request");

      StatusBSO.insertStatusForRequestId(parsedMessage.get(Constants.Request.REQUESTID),
          parsedMessage.get(Constants.Request.VALIDATIONTYPE),
          Constants.RequestStatus.ERROR,null,
          parsedMessage.get(Constants.Request.ENVIRONMENTNAME),
          parsedMessage.get(Constants.Request.DOCUMENTID), "Invalid Request");

    }

    logger.debug(LoggerConstants.LOG_MAXIQAPI, "<< process");

  }
  
  public static void main (String[] args)
  {
	 // do nothing  
  }
}