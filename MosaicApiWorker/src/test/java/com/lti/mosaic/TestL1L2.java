package com.lti.mosaic;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.junit.Test;

import in.lnt.utility.general.CacheLoadListener;
import in.lti.mosaic.api.base.constants.Constants;
import in.lti.mosaic.api.base.serializer.ObjectSerializationHandler;
import com.lti.mosaic.api.worker.processor.CoreProcessor;

/**
 * @author rushikesh
 *
 */
public class TestL1L2 {
  
	public static void main(String[] args) {
		
	CacheLoadListener.loadCache();
	String reqid ="api_uuis1_013";
		
    CoreProcessor l1 = new CoreProcessor();
    Map<String, String> inputMap = new HashMap<String, String>();
    inputMap.put(Constants.Request.ENVIRONMENTNAME, "DEV");
    //Working
    inputMap.put(Constants.Request.DOCUMENTID, "5a7c5ef601648000184cad89");
    inputMap.put(Constants.Request.REQUESTID, reqid+"a");
    inputMap.put(Constants.Request.VALIDATIONTYPE, "L1");    
    
    CoreProcessor l2 = new CoreProcessor();
    Map<String, String> inputMap2 = new HashMap<String, String>();
    inputMap2.put(Constants.Request.ENVIRONMENTNAME, "DEV");
    inputMap2.put(Constants.Request.DOCUMENTID, "5a537a61f2be1500186b5ed5");
    inputMap2.put(Constants.Request.REQUESTID, reqid+"b");
    inputMap2.put(Constants.Request.VALIDATIONTYPE, "L1");

    l1.process(ObjectSerializationHandler.toString(inputMap));
    l2.process(ObjectSerializationHandler.toString(inputMap2));
    
    System.out.println("TestL1L2 finished.");
  }

}