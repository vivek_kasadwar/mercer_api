{
    "entities": [
        {
            "sections": [
                {
                    "sectionCode": "incumbent_data",
                    "data": [
                        {
                            "YOUR_EEID": "1",
                            "question2": "",
                            "$$id": "58d0360baa2ae02cc4723431",
                            "question1": {
                                "ques1": "10",
                                "ques2": "10",
                                "ques3": "10"
                            },
                            "EMP006": "EMP_006",
                            "validationResults": [
                                {
                                    "dependentColumns": [],
                                    "expression": "",
                                    "referenceTableName": "ref_table_name",
                                    "validationType": "rangeValidationRefTable",
                                    "mda": "DECODE(this.question1.ques1 >= 5,this.question1.ques1,50);",
                                    "_id": "59df5c44527b5be1ac230224",
                                    "message": "value is less than minimum required Error value",
                                    "category": "input_data",
                                    "field": "question1",
                                    "errorType": "ERROR",
                                    "dimension": "ques1"
                                },
                                {
                                    "dependentColumns": [],
                                    "expression": "",
                                    "referenceTableName": "ref_table_name",
                                    "validationType": "rangeValidationRefTable",
                                    "mda": "DECODE(this.question1.ques1 >= 5,this.question1.ques1,50);",
                                    "_id": "59df5c44527b5be1ac230224",
                                    "message": "value is less than minimum required Error value",
                                    "category": "input_data",
                                    "field": "question1",
                                    "errorType": "ERROR",
                                    "dimension": "ques3"
                                },
                                {
                                    "dependentColumns": [],
                                    "expression": "",
                                    "referenceTableName": "ref_table_name",
                                    "validationType": "rangeValidationRefTable",
                                    "mda": "DECODE(this.question1.ques1 >= 5,this.question1.ques1,50);",
                                    "_id": "59df5c44527b5be1ac230224",
                                    "message": "value is less than minimum required Error value",
                                    "category": "input_data",
                                    "field": "question1",
                                    "errorType": "ERROR",
                                    "dimension": "ques2"
                                },
                                {
                                    "dependentColumns": [],
                                    "expression": "",
                                    "referenceTableName": "ref_table_name",
                                    "validationType": "rangeValidationRefTable",
                                    "mda": "clear_value",
                                    "_id": "59df5c44527b5be1ac230224",
                                    "message": "value is less than minimum required Error value",
                                    "category": "input_data",
                                    "field": "question2",
                                    "errorType": "ERROR"
                                }
                            ]
                        },
                        {
                            "YOUR_EEID": "2",
                            "question2": "",
                            "$$id": "58d03653aa2ae02cc4723432",
                            "question1": {
                                "ques1": "10",
                                "ques2": "10",
                                "ques3": "10"
                            },
                            "EMP006": "EMP_006",
                            "validationResults": [
                                {
                                    "dependentColumns": [],
                                    "expression": "",
                                    "referenceTableName": "ref_table_name",
                                    "validationType": "rangeValidationRefTable",
                                    "mda": "DECODE(this.question1.ques1 >= 5,this.question1.ques1,50);",
                                    "_id": "59df5c44527b5be1ac230224",
                                    "message": "value is less than minimum required Error value",
                                    "category": "input_data",
                                    "field": "question1",
                                    "errorType": "ERROR",
                                    "dimension": "ques1"
                                },
                                {
                                    "dependentColumns": [],
                                    "expression": "",
                                    "referenceTableName": "ref_table_name",
                                    "validationType": "rangeValidationRefTable",
                                    "mda": "DECODE(this.question1.ques1 >= 5,this.question1.ques1,50);",
                                    "_id": "59df5c44527b5be1ac230224",
                                    "message": "value is less than minimum required Error value",
                                    "category": "input_data",
                                    "field": "question1",
                                    "errorType": "ERROR",
                                    "dimension": "ques3"
                                },
                                {
                                    "dependentColumns": [],
                                    "expression": "",
                                    "referenceTableName": "ref_table_name",
                                    "validationType": "rangeValidationRefTable",
                                    "mda": "DECODE(this.question1.ques1 >= 5,this.question1.ques1,50);",
                                    "_id": "59df5c44527b5be1ac230224",
                                    "message": "value is less than minimum required Error value",
                                    "category": "input_data",
                                    "field": "question1",
                                    "errorType": "ERROR",
                                    "dimension": "ques2"
                                },
                                {
                                    "dependentColumns": [],
                                    "expression": "",
                                    "referenceTableName": "ref_table_name",
                                    "validationType": "rangeValidationRefTable",
                                    "mda": "clear_value",
                                    "_id": "59df5c44527b5be1ac230224",
                                    "message": "value is less than minimum required Error value",
                                    "category": "input_data",
                                    "field": "question2",
                                    "errorType": "ERROR"
                                }
                            ]
                        }
                    ]
                },
                {
                    "sectionCode": "company_data",
                    "data": [
                        {
                            "YOUR_EEID": "3",
                            "question2": "20",
                            "$$id": "58d0360baa2ae02cc4723431",
                            "question1": "",
                            "EMP006": "EMP_006",
                            "EXCLUDE_FLAG": "Y",
                            "validationResults": [
                                {
                                    "dependentColumns": [],
                                    "expression": "",
                                    "referenceTableName": "ref_table_name",
                                    "validationType": "rangeValidationRefTable",
                                    "mda": "clear_value",
                                    "_id": "59df5c44527b5be1ac230224",
                                    "message": "value is less than minimum required Error value",
                                    "category": "input_data",
                                    "field": "question1",
                                    "errorType": "ERROR"
                                },
                                {
                                    "dependentColumns": [],
                                    "expression": "",
                                    "referenceTableName": "ref_table_name",
                                    "validationType": "rangeValidationRefTable",
                                    "mda": "exclude_row",
                                    "_id": "59df5c44527b5be1ac230224",
                                    "message": "value is less than minimum required Error value",
                                    "category": "input_data",
                                    "field": "question2",
                                    "errorType": "ERROR"
                                }
                            ]
                        },
                        {
                            "YOUR_EEID": "4",
                            "question2": "16",
                            "$$id": "58d03653aa2ae02cc4723432",
                            "question1": "",
                            "EMP006": "EMP_006",
                            "EXCLUDE_FLAG": "Y",
                            "validationResults": [
                                {
                                    "dependentColumns": [],
                                    "expression": "",
                                    "referenceTableName": "ref_table_name",
                                    "validationType": "rangeValidationRefTable",
                                    "mda": "clear_value",
                                    "_id": "59df5c44527b5be1ac230224",
                                    "message": "value is less than minimum required Error value",
                                    "category": "input_data",
                                    "field": "question1",
                                    "errorType": "ERROR"
                                },
                                {
                                    "dependentColumns": [],
                                    "expression": "",
                                    "referenceTableName": "ref_table_name",
                                    "validationType": "rangeValidationRefTable",
                                    "mda": "exclude_row",
                                    "_id": "59df5c44527b5be1ac230224",
                                    "message": "value is less than minimum required Error value",
                                    "category": "input_data",
                                    "field": "question2",
                                    "errorType": "ERROR"
                                }
                            ]
                        }
                    ]
                }
            ],
            "contextData": {
                "uniqueIdColumnCode": "YOUR_EEID",
                "companyId": "59c3eac0fc22fa6928d09aa4",
                "campaignId": "59ca2a66c1e15a565af88407",
                "grpCode": "",
                "companyName": "Exadel",
                "cpyCode": "",
                "industry": {
                    "superSector": "CG",
                    "subSector": "2670",
                    "sector": "101"
                },
                "sectionId": "pe3707Section",
                "orgSize": {
                    "59c3eac0fc22fa6928d09aa4": null
                },
                "ctryCode": "PL"
            }
        }
    ]
}