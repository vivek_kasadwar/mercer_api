package com.lti.mosaic.function.def;

import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.language.Soundex;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StringFunctions {
	private static final Logger logger = LoggerFactory.getLogger(StringFunctions.class);

	private static final Pattern LTRIM = Pattern.compile("^\\s+");
	private static final Pattern RTRIM = Pattern.compile("\\s+$");

	private StringFunctions() {
	}

	public static Value concat(String arg1, String arg2) {
		return new Value(StringUtils.join(arg1, arg2));
	}

	public static Value length(Value arg1) {
		
		if ( null== arg1 )
			return new Value(0);
		
		if ( arg1.isArray()) {
		    return  arg1.asArray().length == 0 ? new Value(0) : new Value(BigDecimal.valueOf(arg1.asArray().length));
		    
		}else {
			return new Value(BigDecimal.valueOf(StringUtils.length(arg1.toString())));
		}	
	}

	public static Value repeat(String arg1, Integer arg2) {
		return new Value(StringUtils.repeat(arg1, arg2));
	}

	public static Value lower(String arg1) {
		return new Value(StringUtils.lowerCase(arg1));
	}

	public static Value upper(String arg1) {
		return new Value(StringUtils.upperCase(arg1));
	}

	public static Value instr(String arg1, String arg2) {
		return new Value(BigDecimal.valueOf(StringUtils.indexOf(arg1, arg2) + (long) 1));
	}

	public static Value locate(String arg1, String arg2, Integer arg3) {
		String str = arg2;
		str = StringUtils.substring(str, arg3);
		int len = StringUtils.indexOf(str, arg1);
		if (len > -1) {
			len += arg3 + 1;
		} else
			len = 0;

		return new Value(BigDecimal.valueOf(len));
	}

	public static Value reverse(String arg1) {
		Value result = new Value(StringUtils.reverse(arg1));
		logger.debug(" << perform() - reverse : {}", result);
		return result;
	}

	public static Value rpad(String arg1, Integer arg2, String arg3) {
		Value result =  new Value(StringUtils.rightPad(arg1, arg2, arg3));
		logger.debug(" << perform() - rpad : {}", result);
		return result;
	}

	public static Value lpad(String arg1, Integer arg2, String arg3) {
		Value result =  new Value(StringUtils.leftPad(arg1, arg2, arg3));
		logger.debug(" << perform() - lpad : {}",result);
		return result;
	}

	public static Value ltrim(String arg1) {
		Value result =  new Value(LTRIM.matcher(arg1).replaceAll(""));
		logger.debug(" << perform() - ltrim : {}", result);
		return result;
	}

	public static Value trim(String arg1) {
		Value result = new Value(arg1.trim());
		logger.debug(" << perform() - trim : {}", result);
		return result;
	}

	public static Value rtrim(String arg1) {
		Value result =  new Value(RTRIM.matcher(arg1).replaceAll(""));
		logger.debug(" << perform() - rtrim : {}",result);
		return new Value(RTRIM.matcher(arg1).replaceAll(""));
	}

	public static Value initCap(String arg1) {
		Value result = new Value(Character.toUpperCase(arg1.charAt(0)) + ((arg1.length() > 1) ? arg1.substring(1) : ""));
		logger.debug(" << perform() - initCap : {}", result);
		return result;
	}

	public static Value soundex(String arg1) {
		Value result = new Value(new Soundex().soundex(arg1));
		logger.debug(" << perform() - soundex : {}", result);
		return result;
	}

	public static Value levenshtein(String arg1, String arg2) {
		int diff = 0;
		int len0 = arg1.length();
		int len1 = arg2.length();
		String str0 = "";
		String str1 = "";
		if (len0 > len1) {
			diff = len0 - len1;
			str0 = arg2;
			str1 = arg1;
		} else {
			diff = len1 - len0;
			str0 = arg1;
			str1 = arg2;
		}
		for (int i = 0; i < str0.length(); i++) {
			if (str0.charAt(0) != str1.charAt(0))
				diff++;
		}
		logger.debug(" << perform() - levenshtein : {}", diff);
		return new Value(BigDecimal.valueOf(diff));
	}

	public static Value regexMatch(String regex, String inputString) {
		Pattern p = null;
		Matcher m = null;
		if (inputString == null || regex == null) {
			logger.debug(" << perform() - regexMatch : {}", false);
			return new Value(false);
		}
		p = Pattern.compile(regex);
		m = p.matcher(inputString);
		logger.debug(" << perform() - regexMatch : {}", m.matches());
		return new Value(m.matches());
	}

	public static Value isBlank(Value arg1) {
		boolean result = false;

		if (arg1.isArray()) {
			result = isBlankForArray(arg1);
		} else {
			if (StringUtils.isEmpty(arg1.toString()))
				result = true;
			else
				result = false;
		}
		logger.debug(" << perform() - isBlank : {}", result);
		return new Value(result);
	}

	/**
	 * @param arg1
	 * @return
	 */
	private static boolean isBlankForArray(Value arg1) {
		boolean result;
		int count = 0;
		if (arg1.asArray().length == 0) {
			result = true;
		} else {
			for (Value valObj : arg1.asArray()) {
				if (valObj.isNull()) {
					count++;
				}
			}
			if (count == arg1.asArray().length) {
				result = true;
			} else {
				result = false;
			}
		}
		return result;
	}

	public static Value isNotBlank(Value arg1) {
		boolean result = false;

		if (arg1.isArray()) {
			result = isNotBlankForArray(arg1);
		} else {
			if (!StringUtils.isEmpty(arg1.toString()))
				result = true;
			else
				result = false;
		}
		logger.debug(" << perform() - isNotBlank : {}", result);
		return new Value(result);
	}

	/**
	 * @param arg1
	 * @return
	 */
	private static boolean isNotBlankForArray(Value arg1) {
		boolean result;
		int count = 0;
		if (arg1.asArray().length == 0) {
			result = false;
		} else {
			for (Value valObj : arg1.asArray()) {
				if (valObj.isNull()) {
					count++;
				}
			}
			if (count == arg1.asArray().length) {
				result = false;
			} else {
				result = true;
			}
		}
		return result;
	}

	public static Value substr(Value value, Value value2, Value value3) {
		logger.debug(" >> perform() - substr : Input : {} {} {}", value, value2, value3);
		Value result = mid(value, value2, value3);
		logger.debug(" << perform() - substr : result {}", result);
		return result;
	}

	public static Value substrp(String arg1, Integer arg2, Integer arg3) {
		arg2 = arg2 - 1;
		if (arg1.length() > 0) {
			if (arg3 == 0) {
				return new Value(StringUtils.substring(arg1, arg2));
			} else if (arg2 < arg3) {
				return new Value(StringUtils.substring(arg1, arg2, arg3));
			}
		} else {
			return new Value("");
		}
		return null;
	}

	/**
	 * 
	 * @param value
	 *            - input string
	 * @param value2
	 *            - start (Starting index = 1)
	 * @param value3
	 *            - end
	 * @return string
	 * 
	 *         If only value = null, return null IF only value2 = null && value3 =
	 *         null, return value1; IF only value2 = null, return value2 = 0 IF only
	 *         value3 = null, return value3 = length of value
	 * 
	 */
	public static Value mid(Value value, Value value2, Value value3) {
		boolean start = null != value2;
		boolean end = null != value3;

		if (null != value) {
			if (start && end) {
				return new Value(StringUtils.mid(value.asString(), value2.asInteger().intValue() - 1,
						value3.asInteger().intValue()));
			} else if (start && !end) {
				return new Value(StringUtils.mid(value.asString(), value2.asInteger().intValue() - 1,
						value.asString().length()));
			} else if (!start && end) {
				return new Value(StringUtils.mid(value.asString(), 0, value3.asInteger().intValue()));
			} else {
				return value;
			}
		} else {
			return null;
		}
	}

	public static Value regexpReplace(String arg1, String arg2, String arg3) {
		String value = arg1;
		String regex = arg2;
		String replacewith = arg3;
		Value result = new Value(value.replaceAll(regex, replacewith));
		logger.debug(" << perform() - regexpreplace : {}", result);
		return result;
	}

	public static Value translate(String arg1, String arg2, String arg3) {
		String output = arg1;
		char[] from = arg2.toCharArray();
		char[] to = arg3.toCharArray();
		for (int i = 0; i < from.length; i++) {
			if (to.length > i) {
				output = StringUtils.replaceChars(output, from[i], to[i]);
			}
		}
		logger.debug(" << perform() - translate : {}", output);
		return new Value(output);
	}

	public static Value regexpExtract(String arg1, String arg2, Integer arg3) {
		String subject = arg1;
		String pattern = arg2;
		int index = arg3;
		String evaluateText = regexpExtract(subject, pattern, index);
		logger.debug(" << perform() - regexpExtract : {}", evaluateText);
		return new Value(evaluateText);
	}

	public static Value space(Integer arg1) {
		StringBuilder res = new StringBuilder();
		for (int i = 0; i < arg1; i++) {
			res.append(" ");
		}
		logger.debug(" << perform() - space : {}", res);
		return new Value(res);
	}

	private static String regexpExtract(String subject, String pattern, int index) {
		String extractedString = "";
		if (null != pattern && null != subject) {
			Pattern p = Pattern.compile(pattern);

			Matcher m = p.matcher(subject);

			if (m.find()) {
				extractedString = m.group(index);
			}
		}
		return extractedString;
	}

	public static Value findInSet(Value value, List<Value> valueList) {

		if (null != value && null != valueList) {
			if (value.isArray()) {
				return findInSetForArray(value, valueList);
			} else {
				if (findInsetForValues(value, valueList)) {
					return new Value(true);
				}
			}
		}
		return new Value(false);

	}

	private static boolean findInsetForValues(Value value, List<Value> valueList) {
		if (valueList.size() > 1) {
			valueList.remove(0);
		}
		if (null != valueList.get(0)) {
			Value[] valueArray = valueList.get(0).asArray();
			for (Value valueObject : valueArray) {
				logger.info("findInSet >> valueObject : {}",valueObject);
				if (value.hashCode() == valueObject.hashCode()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @param value
	 * @param valueList
	 * @return
	 */
	private static Value findInSetForArray(Value value, List<Value> valueList) {
		Value[] valueFirstArray = value.asArray();
		Value[] valueSecondArray = valueList.get(1).asArray();

		return new Value(
				isSubset(valueSecondArray, valueFirstArray, valueSecondArray.length, valueFirstArray.length));
	}

	private static boolean isSubset(Value[] arr1, Value[] arr2, int m, int n) {
		int i = 0;
		int j = 0;
		for (i = 0; i < n; i++) {
			for (j = 0; j < m; j++) {
				if (arr2[i].hashCode() == arr1[j].hashCode())
					break;
			}

			/*
			 * If the above inner loop was not broken at all then arr2[i] is not present in
			 * arr1[]
			 */
			if (j == m)
				return false;
		}

		/*
		 * If we reach here then all elements of arr2[] are present in arr1[]
		 */
		return true;
	}

	public static Value split(String arg1, String arg2) {
		String[] splitArray = arg1.split(arg2);
		Value[] resultArray = new Value[splitArray.length];
		for (int i = 0; i < resultArray.length; i++) {
			resultArray[i] = new Value(splitArray[i]);
		}
		return new Value(resultArray);
	}

}
