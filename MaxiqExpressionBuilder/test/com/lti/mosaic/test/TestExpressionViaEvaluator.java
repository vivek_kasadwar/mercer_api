package com.lti.mosaic.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.lti.mosaic.parser.driver.ExpressionEvalutionDriver;
import com.lti.mosaic.parser.exception.ExpressionEvaluatorException;
import com.lti.mosaic.parser.exception.FieldNotValidException;
import com.univocity.parsers.common.processor.RowListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

public class TestExpressionViaEvaluator {

	public static void main(String[] args)
			throws ExpressionEvaluatorException, FieldNotValidException, FileNotFoundException {
		File initialFile = new File("D:\\Projects\\Mercer\\ScriptAndTestcases\\Sample Request\\data.csv");

		InputStream targetStream = new FileInputStream(initialFile);
		CsvParserSettings parserSettings = new CsvParserSettings();
		parserSettings.setLineSeparatorDetectionEnabled(true);
		RowListProcessor rowProcessor = new RowListProcessor();
		parserSettings.setRowProcessor(rowProcessor);	
		parserSettings.setHeaderExtractionEnabled(true);
		CsvParser csvParser = new CsvParser(parserSettings);
		csvParser.parse(targetStream);
		String[] headers = rowProcessor.getHeaders();
		ArrayList<String[]> data = (ArrayList<String[]>) rowProcessor.getRows();
		data.add(0, headers);
		System.out.println("Data Size: " + data.size());

		ExpressionEvalutionDriver expressionEvalutionDriver = new ExpressionEvalutionDriver();
		expressionEvalutionDriver.setCsvData(data, null);
		Map<String, Object> params = new HashMap<>();
		System.out.println(expressionEvalutionDriver.expressionEvaluatorWithObject(params,
				"XCOUNT(\"EMP_038\",\" YOUR_EEID == \\\"\\\"\" , \"CONSOL_ALLOW\");", false));
	}

}
