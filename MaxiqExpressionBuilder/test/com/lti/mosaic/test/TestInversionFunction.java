package com.lti.mosaic.test;

import java.util.ArrayList;
import java.util.List;

import com.lti.mosaic.function.def.InversionAndRangeCheckFunctions;
import com.lti.mosaic.function.def.Value;
import com.lti.mosaic.parser.enums.FunctionEnum;

public class TestInversionFunction {
	public static void main(String[] args) {
		
		FunctionEnum functionName = FunctionEnum.AGGREGATERANGECHECK; 
		List<Value> arguments = new ArrayList<>();
		
		arguments.set(arguments.size() - 2, new Value("[[POS_CLASS, AVG_MONTH_BASE], [51,1012.0], [52, 1013.0], [53, 1014.0], [54, 1015.0], [55, 1016.0], [56, 1017.0], [57, 1018.0], [58, 1019.0], [59, 1020.0], [60, 1021.0], [61, 1022.0], [62, 1023.0], [63, 1024.0], [64, 1025.0],[65, 1026.0], [66, 1027.0], [67, 1028.0], [68, 1029.0], [69,1030.0]]"));
		arguments.set(0,
		 new Value("{\"POS_CLASS\\u001855\":{\"55\":\"25.0\",\"GROUP_BY_COL\":\"POS_CLASS\",\"MONTH_BASE\":\"25.0\",\"SELECT_COL\":\"MONTH_BASE\",\"POS_CLASS\":\"55\",\"COUNT\":2},\"POS_CLASS\\u001852\":{\"GROUP_BY_COL\":\"POS_CLASS\",\"MONTH_BASE\":\"20\",\"SELECT_COL\":\"MONTH_BASE\",\"POS_CLASS\":\"52\",\"COUNT\":2,\"52\":\"20\"}}"));
		//arguments.set(arguments.size() - 2, new Value("[[POS_CODE, AVG_ANNUAL_BASE], [DAW.03.003.M40, 1001.0], [DAW.04.010.M30, 1019.0], [ENS.03.209.M20, 1005.0], [ENS.04.014.I30, 1004.0], [ENS.04.017.I40, 1003.0], [ENS.07.014.E20, 1002.0], [ENS.07.020.I20, 1014.0], [ENS.09.013.P30, 1013.0], [FIN.03.032.I20, 1018.0], [GMA.03.001.E20, 1009.0], [GMA.03.001.I20, 1008.0], [HLT.07.010.M30, 1016.0], [HRM.03.002.M40, 1000.0], [ITC.12.009.E10, 1015.0], [LCQ.03.010.E10, 1007.0], [LCQ.04.024.M40, 1006.0], [RET.03.015.P20, 1010.0], [SCN.04.009.P20, 1012.0], [SCN.10.015.P10, 1011.0], [SMP.12.001.M20, 1017.0]]"));
		
		InversionAndRangeCheckFunctions InversionAndRangeCheckFunctions = new InversionAndRangeCheckFunctions(null, null);
		InversionAndRangeCheckFunctions.executeInversionAndRangeCheckFunctions(functionName, arguments);

	}
}
