package in.lti.mosaic.api.base.configs;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author rushi
 *
 */

public class Cache {

	private Cache() {
		throw new IllegalStateException("Cache class");
	}


  private static Properties config = new Properties();
  private static Properties error = new Properties();
  private static final Logger logger_ = LoggerFactory.getLogger(Cache.class);

  static {

    String base = System.getenv().get(CacheConstants.BASE_PATH);

    if (StringUtils.isBlank(base)) {
      logger_.error("Base Path $API_HOME {} not configured ", base);
    }


    String basePath = base + "/conf/config.properties";
    String errorPath = base + "/conf/error_logs.properties";

    try (FileInputStream fis = FileUtils.openInputStream(new File(basePath));
        FileInputStream fisForError = FileUtils.openInputStream(new File(errorPath));) {

      config.load(fis);
      error.load(fisForError);

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /***
   * Gets the cache value for the key from config.properties
   * 
   * @param key
   * @return
   */
  public static String getProperty(String key) {
    return config.getProperty(key);
  }

  /**
   * Gets the cache value for the key from error_logs.properties
   * 
   * @param key
   * @return
   */
  public static String getPropertyFromError(String key) {
    return error.getProperty(key);
  }

}
