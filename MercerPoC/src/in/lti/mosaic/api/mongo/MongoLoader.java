package in.lti.mosaic.api.mongo;

import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;

import in.lnt.constants.Constants;
import in.lnt.utility.general.Cache;

/**
 * @author rushikesh
 *
 */
public class MongoLoader {

	private MongoLoader() {
		throw new IllegalStateException("MongoLoader class");
	}

	private static final Logger logger = LoggerFactory.getLogger(MongoLoader.class);
	
	private static String requestCollectionName = Cache
			.getMLProperty(in.lnt.utility.constants.CacheConstats.MONGO_REQUEST_COLLECTION_NAME);
	private static String responseCollectionName = Cache
			.getMLProperty(in.lnt.utility.constants.CacheConstats.MONGO_RESPONSE_COLLECTION_NAME);
	public static ObjectMapper mapper = new ObjectMapper();

	/**
	 * 
	 * @param mongoObject
	 */
	public static void dumpDataToMongo(MongoObject mongoObject) {
		
		try {
			long st = System.currentTimeMillis();
			
			String collectionName = null;
			if (StringUtils.equalsIgnoreCase(mongoObject.getObjectType(), Constants.REQUEST)) {
				collectionName = requestCollectionName;
			} else {
				collectionName = responseCollectionName;
			}
			MongoCollection<Document> collection = MongoConnector.getMongoDBConnection().getCollection(collectionName);

			Document doc = new Document();
			doc.put("request_id", mongoObject.getRequestId() + "");
			doc.put("json", mongoObject.getObjectJson());
			doc.put("timeStamp", new BasicDBObject("date", mongoObject.getTimeStamp()));

			collection.insertOne(doc);
			
			logger.debug("Time taken for dump : Request ID : {}, Time: {}", mongoObject.getRequestId(), (System.currentTimeMillis() - st));
			
		} catch (Exception e) {
			logger.error("Error in dumpDataToMongo : {}" , e.getMessage());
		}
	}

}
