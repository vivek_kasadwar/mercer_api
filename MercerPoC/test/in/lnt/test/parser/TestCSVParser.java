package in.lnt.test.parser;

import java.util.HashMap;
import java.util.Map;

import com.lti.mosaic.parser.driver.ExpressionEvalutionDriver;
import com.lti.mosaic.parser.exception.ExpressionEvaluatorException;
import com.lti.mosaic.parser.exception.FieldNotValidException;

import in.lnt.parser.CSVParser;
import junit.framework.TestCase;

public class TestCSVParser extends TestCase{
   public void testCSVParser() throws ExpressionEvaluatorException, FieldNotValidException{
	   
	   ExpressionEvalutionDriver expressionEvalutionDriver = new ExpressionEvalutionDriver();

	   
	   Map<String, Object> paramValues = new HashMap<String, Object>();
		paramValues.put("{input}", "Mumbai(W)");

		String expression = "upper({input})";
		System.out.println("Upper : " + expressionEvalutionDriver.expressionEvaluatorWithObject(paramValues, expression, false));

   }
}
